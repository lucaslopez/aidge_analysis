"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from aidge_metrics.logic.utils import operators,register_func
from aidge_metrics.generic_node.node import GenericNode

@register_func(operators,"MatMul")
class MatmulNode(GenericNode):
    """Class of the operator Matrix Multiplication"""
    nb_inst=0
    #nb_params defaulted to 0
    nb_params=0

    def __init__(self,
                 model_format:str,
                 inherit_params:dict,
                 node_inst_list:list,
                 initialisation_dims:list):
        """initialisation function of the class MatMul, attribution of all parameters propre to the Matrix Multiplication operator

        :param model_format: name of the format of the model
        :type model_format: str
        :param inherit_params: dictionary of parameters to be inherited, added as attributes of the instance
        :type inherit_params: dict
        :param node_inst_list: list of previously declared nodes intances, useful to retrieve parents
        :type node_inst_list: list
        :param initialisation_dims: dimensions of the initial data input
        :type initialisation_dims: list
        """
        GenericNode.__init__(self,model_format,inherit_params,node_inst_list,initialisation_dims)
        self.name="MatMul_"+str(self.nb_inst)
        type(self).nb_inst+=1
        self.add_out_dims()

    def add_out_dims(self):
        """method to get the output dimensions of the operator"""
        diff=len(self.in_dims)-2
        self.out_dims=[None]*max(len(self.in_dims),len(self.in_dims2))

        for i in range(diff):
            self.out_dims[i]=self.in_dims[i]
        self.out_dims[diff]=self.in_dims[diff]
        self.out_dims[diff+1]=self.in_dims2[diff+1]

    @property
    def nb_op(self) -> int:
        """method to calculate the number of operations of the operator

        :return: number of operations
        :rtype: int
        """
        total=1
        diff=len(self.out_dims)-2

        for i in range(diff):
            total=total*self.out_dims[i]
        return self.in_dims[diff+1]*self.in_dims2[diff+1]*self.in_dims[diff]
