"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from aidge_metrics.logic.utils import operators,register_func
from aidge_metrics.generic_node.node import GenericNode

@register_func(operators,"Relu")
class ReluNode(GenericNode):
    """Class of the operator Relu"""
    nb_inst=0
    #nb_params defaulted to 0
    nb_params=0

    def __init__(self,
                 model_format:str,
                 inherit_params:dict,
                 node_inst_list:list,
                 initialisation_dims:list):
        """initialisation function of the class ReluNode, attribution of all parameters propre to the Relu operator

        :param model_format: name of the format of the model
        :type model_format: str
        :param inherit_params: dictionary of parameters to be inherited, added as attributes of the instance
        :type inherit_params: dict
        :param node_inst_list: list of previously declared nodes intances, useful to retrieve parents
        :type node_inst_list: list
        :param initialisation_dims: dimensions of the initial data input
        :type initialisation_dims: list
        """
        GenericNode.__init__(self,model_format,inherit_params,node_inst_list,initialisation_dims)
        self.name="Relu_"+str(self.nb_inst)
        type(self).nb_inst+=1
        self.add_out_dims()

    def add_out_dims(self):
        """method to get the output dimensions of the operator"""
        self.out_dims=self.in_dims

    @property
    def nb_op(self) -> int:
        """method to calculate the number of operations of the operator

        :return: number of operations
        :rtype: int
        """
        total=1
        for i in self.in_dims:
            total=total*i
        return total
