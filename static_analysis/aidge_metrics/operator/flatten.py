"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from aidge_metrics.logic.utils import operators,register_func
from aidge_metrics.generic_node.node import GenericNode

@register_func(operators,"Flatten")
class FlattenNode(GenericNode):
    """Class of the operator Flatten"""
    nb_inst=0
    #nb_op and nb_params defaulted to 0
    nb_op=0
    nb_params=0

    def __init__(self,
                 model_format:str,
                 inherit_params:dict,
                 node_inst_list:list,
                 initialisation_dims:list):
        """initialisation function of the class FlattenNode, attribution of all parameters propre to the Flatten operator

        :param model_format: name of the format of the model
        :type model_format: str
        :param inherit_params: dictionary of parameters to be inherited, added as attributes of the instance
        :type inherit_params: dict
        :param node_inst_list: list of previously declared nodes intances, useful to retrieve parents
        :type node_inst_list: list
        :param initialisation_dims: dimensions of the initial data input
        :type initialisation_dims: list
        """
        GenericNode.__init__(self,model_format,inherit_params,node_inst_list,initialisation_dims)
        self.name="Flatten_"+str(self.nb_inst)
        type(self).nb_inst+=1
        self.add_out_dims()

    def add_out_dims(self):
        """method to get the output dimensions of the operator"""
        total=1
        for in_d in self.in_dims:
            total=total*in_d
        self.out_dims=[1,1,1,total]
