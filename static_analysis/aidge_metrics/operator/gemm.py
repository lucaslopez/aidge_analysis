"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from aidge_metrics.logic.utils import operators,register_func
from aidge_metrics.generic_node.node import GenericNode

@register_func(operators,"Gemm")
class GemmNode(GenericNode):
    """Class of the operator Gemm"""
    nb_inst=0
    #dictionary with the function used to get the unique parameters of the node
    unique_parameters_functions={}

    def __init__(self,
                 model_format:str,
                 inherit_params:dict,
                 node_inst_list:list,
                 initialisation_dims:list):
        """initialisation function of the class GemmNode, attribution of all parameters propre to the Gemm operator

        :param model_format: name of the format of the model
        :type model_format: str
        :param inherit_params: dictionary of parameters to be inherited, added as attributes of the instance
        :type inherit_params: dict
        :param node_inst_list: list of previously declared nodes intances, useful to retrieve parents
        :type node_inst_list: list
        :param initialisation_dims: dimensions of the initial data input
        :type initialisation_dims: list
        """
        GenericNode.__init__(self,model_format,inherit_params,node_inst_list,initialisation_dims)
        self.name="Gemm_"+str(self.nb_inst)
        type(self).nb_inst+=1
        self.unique_parameters_functions[model_format](self)
        self.add_out_dims()

    def add_out_dims(self):
        """method to get the output dimensions of the operator"""
        self.out_dims=[1,1,1,self.weight_dims[0]]

    @property
    def nb_op(self) -> int:
        """method to calculate the number of operations of the operator

        :return: number of operations
        :rtype: int
        """
        return self.out_dims[3]*self.in_dims[3]

    @property
    def nb_params(self) -> int:
        """method to calculate the number of parameters in the operator

        :return: number of parameters
        :rtype: int
        """
        return self.out_dims[3]*(self.in_dims[3]+1)
