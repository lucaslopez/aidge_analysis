"""Module containing the method to get the basic attributes of a generic node in the onnx format"""
from aidge_metrics.generic_node.onnx.generic_params import onnx_generic_params
