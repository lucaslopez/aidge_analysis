"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from aidge_metrics.generic_node.node import GenericNode
from aidge_metrics.logic.utils import register_func

@register_func(GenericNode.parameters_functions,"aidge")
def aidge_generic_params(class_:GenericNode) -> None:
    """generic or unknown operator function to recover basic parameters from the aidge node.

    :param class_: instance of the 'GenericNode' class or it's subclasses to add the parameters to.
    :type class_: GenericNode
    """
    #in_model_name and in_model_op_type indicate its name and op_type as it is in the model
    #before the intermediary representation
    if not hasattr(class_,"in_model_name"):
        class_.in_model_name=class_.node.name()
    class_.in_model_op_type=class_.node.type()

    #test of parents needed because of the get_input_dims called at the initialization or in preprocessing
    if not hasattr(class_,"parents"):
        paren=class_.node.get_parents()
        #make a list of the parents, with their name and not references
        class_.parents=list(map(lambda x : None if x is None else x.name(),paren))
