"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from aidge_metrics.logic.utils import get_inputs_dims

#Generic node or operator, all of the other operator classes inherit from generic_node
#This class is also used if a operator is not recognized
#To not interrupt the process, when this happens it will default all metrics as 0 and have out_dims=in_dims
class GenericNode():
    """generic or unknown operator class, will be inherited by all other operators.
    Usage of this class would entail the presence of an unknown, not supported operator.
    """
    parameters_functions={}#dict with ideally format of the model and dict with parameters
    def __init__(self,
                 model_format:str,
                 inherit_params:dict,
                 node_inst_list:list,
                 initialisation_dims:list,
                 generic_mode:bool=False):
        """initialisation function of GenericNode, incorporation of inherited parameters.

        :param model_format: name of the format of the model
        :type model_format: str
        :param inherit_params: dictionary of parameters to be inherited, added as attributes of the instance
        :type inherit_params: dict
        :param node_inst_list: list of previously declared nodes intances, useful to retrieve parents
        :type node_inst_list: list
        :param initialisation_dims: dimensions of the initial data input
        :type initialisation_dims: list
        :param generic_mode: boolean that indicates if this init was executed with the objective of creating a GenericNode or a more specific operator node, defaults to False
        :type generic_mode: bool, optional
        """
        #puts all of the "inherited" params as attributes of the instance
        for inherit_key in inherit_params:
            setattr(self,inherit_key,inherit_params[inherit_key])

        #set base parameters, ex: name of operator, op-type etc
        self.parameters_functions[model_format](self)

        self.format=model_format
        self.nb_inputs=1
        #get input dimensions of the operator
        if not hasattr(self,"in_dims"):
            get_inputs_dims(self,node_inst_list,initialisation_dims)

        #generic_mode is what is used to indicate that the node is generic/ unknown operator
        #this means that out_dims=in_dims
        if generic_mode:
            self.add_out_dims()
            self.nb_op=0
            self.nb_params=0

    #calc of out_dims
    def add_out_dims(self):
        """method to add the output dimensions as attribute."""
        self.out_dims=self.in_dims

    #method used to reset the number of instance of a class
    def reset_nb_inst(self):
        """method that resets the class variable instance counter of their class."""
        type(self).nb_inst=0
