"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from typing import Generator,Union
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

from matplotlib.backends.backend_pdf import PdfPages

sns.set_theme(rc = {'figure.figsize':(15,10)})

def list_to_generator(list_to_gen:list) -> Generator[Union[str,plt.Axes],None,None]:
    """generator function that transform a list of unknown depth into a generator.

    :param list_to_gen: list to be used for the generator
    :type list_to_gen: list
    :yield: element of 'list_to_gen'
    :rtype: Generator[Union[str,plt.Axes],None,None]
    """
    if (hasattr(list_to_gen,'__iter__') and (not isinstance(list_to_gen,str))):
        for ax in list_to_gen:
            yield from list_to_generator(ax)
    else:
        yield list_to_gen

#figure and subplot generator
def figure_gen(nb_figures:int,dataframe:pd.DataFrame,nb_operators:int) -> list:
    """function that creates the matplotlib figure and subplots if necessary

    :param nb_figures: number of figures per metric
    :type nb_figures: int
    :param dataframe: dataframe contenant toutes les donnees des metriques
    :type dataframe: pd.DataFrame
    :param nb_operators: total number of operators in the model
    :type nb_operators: int
    :return: list of instances of the class 'OutFigures' with all the data of the figures and subfigures
    :rtype: list
    """
    #figures and axes list:
    l_fig_axes=[]


    subplot_needed = (nb_figures != 4)

    for id_fig in (range(nb_figures)):
        plt.figure(id_fig)

        if subplot_needed:
            plots_per_fig = 4//nb_figures
            f,axes = plt.subplots((plots_per_fig%2==0) +1,(plots_per_fig//4)+1)

            l_fig_axes.append(OutFigures(f,axes,id_fig,dataframe,nb_operators))
        else:
            l_fig_axes.append(OutFigures(plt.figure(id_fig),plt.figure(id_fig).axes,id_fig,dataframe,nb_operators))
    return l_fig_axes

#generates and draws in the plots
def ploting(fig_per_metric:int,
            output_format:str,
            dataframe:pd.DataFrame,
            graphs_generator:Generator,
            nb_operators:int,
            save_path:str=None,
            pdf_file:PdfPages=None) -> Union[None,PdfPages]:
    """Draws the graphs and plots on the respective figures

    :param fig_per_metric: number of figures per metric
    :type fig_per_metric: int
    :param output_format: format of the output files
    :type output_format: str
    :param dataframe: dataframe contenant toutes les donnees des metriques
    :type dataframe: pd.DataFrame
    :param graphs_generator: generator returning the graph to be drawn each time it is called
    :type graphs_generator: Generator
    :param nb_operators: total number of operators
    :type nb_operators: int
    :param save_path: path to where the output file will be saved, defaults to None
    :type save_path: str, optional
    :param pdf_file: pdf file where everything is being saved, defaults to None
    :type pdf_file: PdfPages, optional
    :return: PdfPages object
    :rtype: Union[None,PdfPages]
    """
    list_figs=figure_gen(fig_per_metric,dataframe,nb_operators)

    for fig_i,fig_inst in enumerate(list_figs):
        #activating current figure
        plt.figure(fig_inst.fig)

        if fig_inst.multiplot:
            for i in range(fig_inst.nb_axs):
                getattr(fig_inst,next(graphs_generator))()
        else:
            getattr(fig_inst,next(graphs_generator))()
        plt.tight_layout()

        if output_format == "pdf":
            pdf_file.savefig(fig_inst.fig)
        elif fig_inst.nb_axs == 4:
            plt.savefig(save_path+"."+output_format,format=output_format)
        else:
            plt.savefig(save_path+"_"+str(fig_i)+"."+output_format,format=output_format)

        plt.close(fig_inst.fig)

    if output_format=="pdf":
        return pdf_file

def outputs(l_metrics_values:list,
            l_metric_types:list,
            save_path:str,
            output_format:str,
            plot_per_fig:int,
            graph_types:list,
            nb_operators:int) -> None:
    """Manages the creation of the pdf output

    :param l_metrics_values: list of all the calculated metrics values
    :type l_metrics_values: list
    :param l_metric_types: list with the name of the metrics to be put in a graph
    :type l_metric_types: list
    :param save_path: path to where the ouput file will be saved
    :type save_path: str
    :param output_format: format of the output files
    :type output_format: str
    :param plot_per_fig: number of graphs or plots to be generated in a single figure or page
    :type plot_per_fig: int
    :param graph_types: ordered list of the graphs to be drawn in each figure
    :type graph_types: list
    :param nb_operators: total number of operators
    :type nb_operators: int
    """
    #transforming into a pandas dataframe
    metrics_dataframe = pd.DataFrame(l_metrics_values,columns = ["node","metric","operator","metric_value"])
    fig_per_metric = 4//plot_per_fig

    if output_format == "pdf":
        pdf_file=PdfPages(save_path+".pdf")

    #loop for each metric
    for metric_type in l_metric_types:
        #extraction of the relevant portion of the dataframe; only including the current metric
        df=metrics_dataframe[metrics_dataframe["metric"]==metric_type].loc[:,["node","operator","metric_value"]].copy()
        #various number for reference; printing or other calculations
        sum_metrics=float(df.loc[:,"metric_value"].sum())
        #adding new colmn to the dataframe; percentage of total
        df.loc[:,"Percent of total"]=df.loc[:,"metric_value"].div(sum_metrics)

        plots_gen = list_to_generator(graph_types.copy())

        if output_format == "pdf":
            pdf_file = ploting(fig_per_metric,output_format,df,plots_gen,nb_operators,pdf_file=pdf_file)
        else:
            ploting(fig_per_metric,output_format,df,plots_gen,nb_operators,save_path)
    if output_format == "pdf":
        pdf_file.close()
    return None

#each instance of OutFigures should represent a figure
#could be a multiplot with subplots or a simple one
class OutFigures():
    """Class that manages the creation and ploting of the pdf file.
    Each instance of this calss represents a figure or page on the pdf."""
    def __init__(self,figure:plt.Figure,
                 fig_axes:list,
                 fig_id:int,
                 dataframe:pd.DataFrame,
                 nb_operators:int):
        """initialisation method of the class OutFigures, definition of all attributes

        :param figure: matplotlib figure to plot onto
        :type figure: plt.Figure
        :param fig_axes: list of the different matplotlib axes in case of a multiplot, [] in the case of a single figure plot
        :type fig_axes: list
        :param fig_id: matplotlib figure number or id
        :type fig_id: int
        :param dataframe: pandas dataframe containing all metric values
        :type dataframe: pd.DataFrame
        :param nb_operators: total number of operators
        :type nb_operators: int
        """
        self.fig = figure
        self.list_axes = fig_axes
        self.fig_id = fig_id
        self.df = dataframe
        self.nb_operators = nb_operators
        #number of axes or subplots
        if len(self.list_axes):
            self.axe_gen = list_to_generator(self.list_axes)
            self.multiplot = True
            self.nb_axs = self.list_axes.size
        else:
            self.multiplot = False
            self.nb_axs=1

    #method used to plot bar graphs
    def barplot(self,y_ax_name:str = "metric_value",log:bool = True):
        """Method used to plot a barplot onto the next free axe or figure

        :param y_ax_name: name of the column that will be used as Y on the graph, defaults to "metric_value"
        :type y_ax_name: str, optional
        :param log: boolean indicating if the scale is logarithmic, defaults to True
        :type log: bool, optional
        """
        #color gradient
        max_metrics_value = float(self.df.loc[:,y_ax_name].max())
        color_gradient = [ (x,1-x,0.1) for x in self.df.loc[:,y_ax_name].div(max_metrics_value)]

        if self.multiplot:
            ax=next(self.axe_gen)
        else:
            ax=None
        plot=sns.barplot(ax=ax,data=self.df,x="node",y=y_ax_name,palette=color_gradient,hue="node",legend=False,errorbar=None,n_boot=0)
        plot.axes.set_xticks(self.df.loc[:,"node"],plot.get_xticklabels(), rotation=60,ha='right',fontsize=(9/self.nb_axs)+(10/(1+(self.nb_operators/10))))
        if log:
            plot.set_yscale("log")

    def barplot_percentage(self):
        """method that calls the barplot method for the precentage values as Y value."""
        self.barplot(y_ax_name="Percent of total",log=False)

    #method to create text with various informations
    def text(self):
        """method used to generate miscellanious text onto a plot.
        The text contains the total value of a metric, max value and mean value.
        """
        total_metrics = float(self.df.loc[:,"metric_value"].sum())
        mean_metrics = float(self.df.loc[:,"metric_value"].mean())
        max_metrics_raw_value = float(self.df.loc[:,"metric_value"].max())
        max_value_operator = self.df.iloc[np.argmax(self.df.loc[:,"metric_value"]),0]
        max_operator_percentage = round(self.df.iloc[np.argmax(self.df.loc[:,"metric_value"]),3]*100,2)

        if self.multiplot:
            current_axe = next(self.axe_gen)

            current_axe.set_axis_off()

            current_axe.text(0, 0.8,f"Total: {total_metrics:,.0f}",fontsize=27)
            current_axe.text(0, 0.6,f"Max value: {max_metrics_raw_value:,.0f}",fontsize=26)
            current_axe.text(0.03, 0.53,f"by {max_value_operator} representing {max_operator_percentage} % of total",fontsize=15)
            current_axe.text(0, 0.3,f"Mean: {mean_metrics:,.0f}",fontsize=26)
        else:
            plt.xticks([])
            plt.yticks([])

            plt.text(0, 0.8,f"Total: {total_metrics:,.0f}",fontsize=27)
            plt.text(0, 0.6,f"Max value: {max_metrics_raw_value:,.0f}",fontsize=26)
            plt.text(0.03, 0.53,f"by {max_value_operator} representing {max_operator_percentage} % of total",fontsize=15)
            plt.text(0, 0.3,f"Mean: {mean_metrics:,.0f}",fontsize=26)

    #method to create a pie chart with the percentage values
    def pieplot(self):
        """Method used to plot a pieplot on the current figure or next avalaible axe"""
        #dataframe copy, to not modify the original
        pie_df = self.df[['operator','Percent of total']].copy()
        #dataframe grouped by operator
        pie_df=pie_df.groupby(['operator'],as_index=False).sum(numeric_only=True)
        #low value metrics will be placed in the 'other' category
        pie_df.loc[pie_df['Percent of total']<0.01,'operator']='Other '
        pie_df=pie_df.groupby(['operator']).sum(numeric_only=True)
        if self.multiplot:
            next(self.axe_gen).pie(data=pie_df,x="Percent of total",autopct='%.2f',labels=pie_df.index.tolist())
        else:
            plt.pie(data=pie_df,x="Percent of total",autopct='%.2f',labels=pie_df.index.tolist())
