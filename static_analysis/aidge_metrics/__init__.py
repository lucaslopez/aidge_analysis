"""Aidge Metrics

Aidge Module focused on benchmarking Deep neural networks.
The benchmarking can be done in different metrics:
- number or operations in MACS
- number of learnable parameters

Metrics numbers are calculated with their theorical formula 
Differences with actual values are to be expected.


This module accepts Aidge and Onnx format models
"""

# from .export import *

__version__ = "0.0.1"

from aidge_metrics import generic_node
from aidge_metrics import operator
from aidge_metrics import formats
from aidge_metrics.logic.iter_graph import operators,aidge_metric
