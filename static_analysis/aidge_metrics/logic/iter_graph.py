"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
import warnings
from typing import Union
import aidge_core
import onnx

from aidge_metrics.logic.utils import sorting_funcs,operators,preprocessing
from aidge_metrics.logic.utils import graph_nodes,arg_verifications
from aidge_metrics.generic_node.node import GenericNode
from aidge_metrics.output.output_func import outputs


def nodes_intermediate_representation(default_nodes_list:list,
                                      graph:Union[onnx.GraphProto,aidge_core.GraphView],
                                      model_format:str,
                                      init_dimensions:list) -> list:
    """function that parses the graph and nodes while transforming them into the intermediary representation

    :param default_nodes_list: list of nodes in the original format 
    :type default_nodes_list: list
    :param graph: graph of the model in the original format
    :type graph: Union[onnx.GraphProto,aidge_core.aidge_core.GraphView]
    :param model_format: name of the format of the model
    :type model_format: str
    :param init_dimensions: dimensions of the initial data input
    :type init_dimensions: list
    :return: returns a list of all the operators instances in the intermediary representation
    :rtype: list
    """
    #get a sorted list of the nodes of a model
    ordered_nodes_list=sorting_funcs[model_format](default_nodes_list)

    #list of all of the instances
    node_inst_list = []

    #list of the operators used, [[type of operator],[an instance]]
    operators_used = [[],[]]
    #creation of all the instances
    for node in ordered_nodes_list:
        #inherit_dict posseses the attributes to be put in the next operator
        #preprocessing prepares the operator if necessary
        inherit_dict = preprocessing[model_format](node)

        inherit_dict["graph"] = graph

        #condition to see if the operator is supported
        if inherit_dict["op_type"] in operators:
            #creation of the operator as a class instance
            instance = operators[inherit_dict["op_type"]](model_format,
                                                            inherit_dict,
                                                            node_inst_list,
                                                            init_dimensions)

            #added to the list of instances
            node_inst_list.append(instance)

            #if it is a new type of operator add it to operators_used
            if not instance.op_type in operators_used[0]:
                operators_used[0].append(instance.op_type)
                operators_used[1].append(instance)

        else:
            op_name = inherit_dict["op_type"]
            warnings.warn(f"{op_name} operator not supported or recognized, will be ignored")
            #operator not recognized so it will be interpreted as a generic operator
            instance = GenericNode(model_format,
                                   inherit_dict,
                                   node_inst_list,
                                   init_dimensions,
                                   generic_mode=True)
            node_inst_list.append(instance)

    #resetting all of the operators classes instances numbers
    #in case these classes are used again in the future
    for op in operators_used[1]:
        op.reset_nb_inst()

    return node_inst_list

def aidge_metric(model:Union[aidge_core.GraphView,str],
                 metrics_list: list=None,
                 model_format: str="aidge" ,
                 init_dimensions: list=None,
                 output_images:bool=False,
                 output_format:str="svg",
                 ignored_operators_list:list=None,
                 ignore_null_metrics:bool=False,
                 save_path:str=None,
                 plot_per_fig:int=4,
                 graph_types_list:list=None) -> list:
    """Primary function of the aidge metrics module.

    :param model: model to be parsed
    :type model: Union[aidge_core.aidge_core.GraphView,str]
    :param metrics_list: list of metrics to be calculated, defaults to ["nb_op"]
    :type metrics_list: list, optional
    :param model_format: name of the format of the model, defaults to "aidge"
    :type model_format: str, optional
    :param init_dimensions: dimensions of the initial data input, defaults to [1, 1, 28, 28]
    :type init_dimensions: list, optional
    :param output_images: boolean that indicates if graphs will be generated onto a file, defaults to False
    :type output_images: bool, optional
    :param output_format: format of the output files, defaults to "svg"
    :type output_format: str, optional
    :param ignored_operators_list: list of the names of the different operators to be ignored during the metric calculation, defaults to []
    :type ignored_operators_list: list, optional
    :param ignore_null_metrics: boolean that indicates that null metric values will be ignored they will not appear in the graphs and output list, defaults to False
    :type ignore_null_metrics: bool, optional
    :param save_path: path to where the ouput file will be saved, defaults to None
    :type save_path: str, optional
    :param plot_per_fig: number of graphs or plots to be generated in a single figure or page, defaults to 4
    :type plot_per_fig: int, optional
    :param graph_types_list: ordered list of the graphs to be drawn in each figure, defaults to ["barplot","barplot_percentage","pieplot","text"]
    :type graph_types_list: list, optional
    :return: list of the metric values of all the operators of the concerned model
    :rtype: list
    """
    #verification of user inputed arguments
    metrics_list,graph_types_list = arg_verifications(save_path,
                                                      output_images,
                                                      init_dimensions,
                                                      ignored_operators_list,
                                                      plot_per_fig,
                                                      graph_types_list,
                                                      metrics_list,
                                                      output_format)

    #getting the graph and nodes list of the model
    graph,default_nodes_list = graph_nodes[model_format](model)

    #initial parsing of the model, getting all relevant parameters
    node_list = nodes_intermediate_representation(default_nodes_list,graph,model_format,init_dimensions)
    metrics_values_list = []
    op_value_list = []

    #loop to calculate the metrics if necessary
    for node in node_list:
        #usage of isinstance not possible because of inheritance, only superclass is to be checked
        if (not node.op_type in ignored_operators_list) and (not type(node) is GenericNode):
            for metric in metrics_list:
                #getting the metric attribute of function
                metric_func = getattr(node,metric,False)


                if ((not metric_func) and (metric_func!=0)):
                    warnings.warn(f"metric {metric} not defined in operator {node.op_type}, value will be defaulted to 0")
                    op_value_list = [node.name,metric,node.op_type,0]
                else:
                    op_value_list = [node.name,metric,node.op_type,metric_func]

                #if told to ignore null values
                #and the value is null or 0 then it will not be stored in the final list
                if not(ignore_null_metrics and (op_value_list[3]==0)):
                    metrics_values_list.append(op_value_list)

    if output_images:
        nb_operators = len(node_list)
        outputs(metrics_values_list,
                metrics_list,
                save_path,
                output_format,
                plot_per_fig,
                graph_types_list,
                nb_operators)

    #returns a list of all the values and "relevant information fo each layer"
    return metrics_values_list
