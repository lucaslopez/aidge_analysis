"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from __future__ import annotations
from typing import Union,Type,TYPE_CHECKING
from functools import wraps
from collections import OrderedDict

#conditional import to prevent circular import
if TYPE_CHECKING:
    from aidge_metrics.generic_node.node import GenericNode

#dictionary of all the operator classes
operators = {}
#dictionary of all the functions to reorganise the list/set of nodes if necessary
sorting_funcs={}
# dictionary of functions used to get the operator type and get other informations if necessary
# sometimes even make the initial parsing of a node:
preprocessing={}
#dictionary used to obtain the graph and initial list of nodes of the model
graph_nodes={}
#dictionary to translate some operators names
#example: even if FC and Gemm are the same they are known as different names in Aidge or Onnx:
operator_translator_dict={"FC":"Gemm","FCOP":"Gemm","GlobalAveragePool":"Global_Pool"}

#lists that contain the graphs or metrics supported, used to verify the contents of the users's input
supported_graphs=["barplot","pieplot","barplot_percentage","text"]
suppported_metrics=["nb_op","nb_params"]
#decorator used to get functions into an specific dictionary: per example; register_func(operators,"conv") will decorate the following class as conv in the dictionary operators
def register_func(dictio:dict,name:str):
    """decorator used to store a function or class as an item in a dictionary

    :param dictio: dictionary where the function or class will be stored
    :type dictio: dict
    :param name: key on the dictionary corresponding to the function or class stored
    :type name: str
    """
    def decorator(operator):
        @wraps(operator)
        def wrapper(*args,**kwargs):
            return operator(*args,**kwargs)
        dictio[name] = operator

        return wrapper
    return decorator

#function used to get the input dimensions of an operator
def get_inputs_dims(node,node_list:list,init_dims:list) -> None:
    """function to locate the dimensions of the input of a node

    :param node: node which the input will be searched
    :type node: _type_
    :param node_list: list of previously declared instances of nodes
    :type node_list: list
    :param init_dims: dimensions of the initial data input
    :type init_dims: list
    """
    inputs_list=[]
    parents_list=node.parents

    if ((parents_list is None) or (node_list==[])):
        node.in_dims=init_dims.copy()
    else:
        for paren in parents_list:
            parent_node=search_parent(paren,node_list)
            if parent_node:
                if hasattr(node,"in_dims"):#if in a previous iteration, an input was found
                    inputs_list.append(parent_node.out_dims)
                else:
                    node.in_dims=parent_node.out_dims
            #if parent_node is False it could mean that the parent was not found
            #or that the parent was just weigths
            #Because of this, no warning or error is produced (impossible to tell the difference)
        for idx,inp in enumerate(inputs_list):
            setattr(node,"in_dims"+str(2+idx),inp)
            node.nb_inputs=node.nb_inputs+1

#function used to search the parent of a node
#search is made within the intermediate representation but with the names of the original representation
def search_parent(paren_name:str,node_list:list) -> Union[Type[GenericNode],bool]:
    """function used to search a specific parent on the list of previously declared operator instances

    :param paren_name: name of the parent to be searched
    :type paren_name: str
    :param node_list: list of previously declared instances of nodes
    :type node_list: list
    :return: parent node if found, False if the parent wasn't found
    :rtype: Union[Type[GenericNode],bool]
    """
    for node in node_list:
        if node.in_model_name==paren_name:
            return node
    return False

def operator_name_normalisation(op_name:str,supported_op_names:list) -> Union[str,bool]:
    """normalisation of the names of operators.

    :param op_name: original name of the operator
    :type op_name: str
    :param supported_op_names: list of current operators names
    :type supported_op_names: list
    :return: corresponding name for the intermediary representation, False if not match was found
    :rtype: Union[str,bool]
    """
    #look in the dictionarie of translations, operator_translator_dict
    if op_name in operator_translator_dict:
        return operator_translator_dict[op_name]
    #in case the name was not found in the dict
    #we will look into the name of the operators themselves in case they are "contained" inbetween
    #per example Conv is present in Conv2d

    #TODO method for better searching may be needed
    for supp_name in supported_op_names:
        if supp_name.lower() in op_name.lower():
            return supp_name
    return False

def arg_verifications(save_path:str,
                      output_images:bool,
                      in_dimensions:list,
                      ignored_operators_list:list,
                      plot_per_fig:int,
                      graph_types_list:list,
                      metrics_list:list,
                      output_format:str) -> list:
    """function that verifies the content of the user inputed arguments

    :param save_path: path to where the ouput file will be saved
    :type save_path: str
    :param output_images: boolean that indicates if graphs will be generated onto a pdf file
    :type output_images: bool
    :param in_dimensions: dimensions of the initial data input
    :type in_dimensions: list
    :param ignored_operators_list: list of the names of the different operators to be ignored during the metric calculation
    :type ignored_operators_list: list
    :param plot_per_fig: number of graphs or plots to be generated in a single figure or page
    :type plot_per_fig: int
    :param graph_types_list: graph_types_list
    :type graph_types_list: list
    :param metrics_list: list of metrics to be calculated
    :type metrics_list: list
    :raises ValueError: if output images are to be generated, a save path is to be expected
    :raises ValueError: 4 distincs graph types must be provided
    :raises ValueError: only supported graph types are to be acceped
    :raises ValueError: all operators in the ignored_operators_list must be valid and supported operators
    :raises ValueError: input data has to have 4 dimensions: Batch , Channels, Height and Width
    :raises ValueError: input data dimensions must be positive integers
    :raises ValueError: metrics_list must only contain supported metrics
    :raises ValueError: number of plots per figure must be 1 , 2 , 4
    :return: metric_list and graph_type_list without duplicates 
    :rtype: list
    """
    #check if a savepath was provided
    if ((save_path is None) and (output_images)):
        raise ValueError("save_path must be provided")

    if output_format not in ("pdf","png","svg"):
        raise ValueError("invalid output format")
    #graph types
    if graph_types_list is None:
        graph_types_list = ["barplot","barplot_percentage","pieplot","text"]
    else:
        #remove duplicates while remaining in order
        graph_types_list=list(OrderedDict.fromkeys(graph_types_list))
        if len(graph_types_list)!=4:
            raise ValueError("invalid graph types, 4 graph types without duplicates must be provided")
        for graph in graph_types_list:
            if not graph in supported_graphs:
                raise ValueError("invalid graph types")
    #operators
    if ignored_operators_list is None:
        ignored_operators_list=[]
    else:
        op_names=operators.keys()
        if ignored_operators_list!=[]:
            for ig_op in ignored_operators_list:
                if not ig_op in op_names:
                    raise ValueError("invalid operators in ignored_operators_list")
    #dimensions
    if in_dimensions is None:
        in_dimensions = [1, 1, 28, 28]
    else:
        if len(in_dimensions)!=4:
            raise ValueError("invalid dimensions, 4 dimensions must be included: NCHW")
        for dim in in_dimensions:
            if (dim<1 or not isinstance(dim,int)):
                raise ValueError("invalid dimensions, dimensions must be positive and a whole number")
    #metrics
    if metrics_list is None:
        metrics_list = ["nb_op"]
    else:
        metrics_list=list(OrderedDict.fromkeys(metrics_list))
        for metr in metrics_list:
            if not metr in suppported_metrics:
                raise ValueError("Invalid metrics, please provide metrics from the supported metrics list")
    #nbfigures
    #the only supported amounts of plots pe figure are 4, 2 or 1
    if plot_per_fig not in (4,2,1):
        raise ValueError("Invalid number of plots per figure, please input one of the following values 4, 2 or 1")

    return metrics_list,graph_types_list
