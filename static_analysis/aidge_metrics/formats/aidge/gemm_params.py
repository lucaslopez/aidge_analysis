"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from aidge_metrics.operator.gemm import GemmNode
from aidge_metrics.logic.utils import register_func

@register_func(GemmNode.unique_parameters_functions,"aidge")
def aidge_gemm(class_:GemmNode) -> None:
    """Gemm operator function to recover additional parameters from the aidge node.

    :param class_: instance of the 'GemmNode' class to add the parameters to.
    :type class_: GemmNode
    """
    class_.weight_dims=class_.node.get_operator().get_input(1).dims()
