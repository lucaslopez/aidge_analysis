"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from aidge_core.aidge_core import Node as AidgNode
from aidge_core.aidge_core import GraphView
from aidge_metrics.logic.utils import register_func,sorting_funcs,preprocessing
from aidge_metrics.logic.utils import operators,operator_name_normalisation,graph_nodes

#function to detect micrographs
@register_func(preprocessing,"aidge")
def micrograph_filter(node:AidgNode) -> dict:
    """Detects aidge Micrographs and splits them.

    :param node: node to check for micrograph
    :type node: 
    """
    is_micrograph=getattr(node.get_operator(),"get_micro_graph",False)
    if is_micrograph:
        return split_micro_graph(node)
    #dict(op_type=operator_name_normalisation(node.type(),operators),node=node,parents=list(map(lambda x : None if x is None else x.name(),node.get_parents())))
    return {"op_type":operator_name_normalisation(node.type(),operators),"node":node,"parents":list(map(lambda x : None if x is None else x.name(),node.get_parents()))}

#splits micrograph and gives the extra attributes to be inherited later
def split_micro_graph(micrograph:AidgNode) -> dict:
    """#splits micrograph and gives the extra attributes to be inherited later.

    :param micrograph: micrograph to be split.
    :type micrograph: 
    """
    microgr_node=micrograph.get_operator()
    name=micrograph.name()
    micro_graph=microgr_node.get_micro_graph()
    microgr_paren=micrograph.get_parents()
    for node in micro_graph.get_nodes():
        if not operator_name_normalisation(node.type(),["Pad"]):
            inner_node=node
        else:
            pad=node.get_operator().get_attr("BeginEndBorders")

    #return dict(in_model_name=name,op_type=operator_name_normalisation(inner_node.type(),operators),node=inner_node,padding=pad,parents=list(map(lambda x : None if x is None else x.name(),microgr_paren)))
    return {"in_model_name":name,"op_type":operator_name_normalisation(inner_node.type(),operators),"node":inner_node,"padding":pad,"parents":list(map(lambda x : None if x is None else x.name(),microgr_paren))}
#function that returns the graph and nodes
@register_func(graph_nodes,"aidge")
def graph_nodes_aidge(model:GraphView):
    """function that returns the graph and list of nodes

    :param model: aidge model which will be used to get the list of nodes and graph
    :type model:
    """
    return model,model.get_nodes()

#code taken from Vincent Templier on Aidge
@register_func(sorting_funcs,"aidge")
def sorting_aidge(nodes:GraphView) -> list:
    """Take an Aidge Graphview 
    and returns a list of nodes topologically sorting
    """

    #nodes = graphview.get_nodes()
    result = []
    visited = set()
    visiting = set()  # To track nodes being currently visited

    def visit(node):
        if node in visiting:
            raise ValueError("Graph contains a cycle")
        if node in visited:
            return
        visiting.add(node)
        for parent in node.get_parents():
            if parent and parent in nodes:
                visit(parent)
        visiting.remove(node)
        visited.add(node)
        if node.type()!="Producer":
            result.append(node)

    for node in nodes:
        visit(node)

    return result
