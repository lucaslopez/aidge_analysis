"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from aidge_metrics.operator.conv import ConvNode
from aidge_metrics.logic.utils import register_func

@register_func(ConvNode.unique_parameters_functions,"aidge")
def aidge_conv(class_:ConvNode) -> None:
    """convolution operator function to recover additional parameters from the aidge node.

    :param class_: instance of the 'ConvNode' class to add the parameters to.
    :type class_: ConvNode.
    """
    class_.kernel=class_.node.get_operator().get_attr("KernelDims")
    class_.dilation=class_.node.get_operator().get_attr("DilationDims")
    class_.stride=class_.node.get_operator().get_attr("StrideDims")
    class_.weight_dims=class_.node.get_operator().get_input(1).dims()
    #initialisation if padding wasn't added before (preprocessing or inherit system)
    if not hasattr(class_,"padding"):
        class_.padding=[0]*len(class_.kernel)*2
