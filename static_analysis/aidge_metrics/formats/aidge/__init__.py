"""module containing every method used to get the attributes of operators on the aidge format
"""
from aidge_metrics.formats.aidge.utils import micrograph_filter,graph_nodes_aidge
from aidge_metrics.formats.aidge.utils import sorting_aidge
from aidge_metrics.formats.aidge.conv_params import aidge_conv
from aidge_metrics.formats.aidge.pool_params import aidge_pool
from aidge_metrics.formats.aidge.gemm_params import aidge_gemm
from aidge_metrics.formats.aidge.dropout_params import aidge_dropout
from aidge_metrics.formats.aidge.add_params import aidge_add
from aidge_metrics.formats.aidge.reshape_params import aidge_reshape
