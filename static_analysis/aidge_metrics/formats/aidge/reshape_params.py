"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from aidge_metrics.operator.reshape import ReshapeNode
from aidge_metrics.logic.utils import register_func

@register_func(ReshapeNode.unique_parameters_functions,"aidge")
def aidge_reshape(class_:ReshapeNode) -> None:
    """Reshape operator function to recover additional parameters from the aidge node.

    :param class_: instance of the 'ReshapeNode' class to add the parameters to.
    :type class_: ReshapeNode
    """
    # #FIXME HARDCODED,bug ON AIDGE NOT LETTING THE EXIT SHAPE ACCESIBLE
    if class_.nb_inst==1:
        class_.new_shape=[1,256]
    else:
        class_.new_shape=[256,10]
    # print(dir(class_.node.get_operator().get_input(0)))
    # class_.new_shape = class_.node.get_operator().get_input(0).dims()
    weight=class_.node.get_operator().get_input(0).dims()
    if weight!=[]:
        class_.weight_in_dims=weight
