"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from aidge_metrics.operator.dropout import DropoutNode
from aidge_metrics.logic.utils import register_func

@register_func(DropoutNode.unique_parameters_functions,"aidge")
def aidge_dropout(class_:DropoutNode) -> None:
    """dropout operator function to recover additional parameters from the aidge node.

    :param class_: instance of the 'DropoutNode' class to add the parameters to.
    :type class_: DropoutNode
    """
    class_.ratio=0#TODO: RATIO NOT ACCESIBLE ON AIDGE YET
