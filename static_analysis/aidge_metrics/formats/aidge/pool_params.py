"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from aidge_metrics.operator.pool import PoolNode
from aidge_metrics.logic.utils import register_func

@register_func(PoolNode.unique_parameters_functions,"aidge")
def aidge_pool(class_:PoolNode) -> None:
    """Pool operator function to recover additional parameters from the aidge node.

    :param class_: instance of the 'PoolNode' class to add the parameters to.
    :type class_: PoolNode
    """
    class_.kernel=class_.node.get_operator().get_attr("KernelDims")

    class_.stride=class_.node.get_operator().get_attr("StrideDims")

    if not hasattr(class_,"padding"):
        class_.padding=[0]*len(class_.kernel)*2
