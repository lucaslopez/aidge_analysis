"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from typing import Union
from onnx import GraphProto,NodeProto,ModelProto
from aidge_metrics.logic.utils import register_func,sorting_funcs,graph_nodes
from aidge_metrics.logic.utils import preprocessing,operator_name_normalisation,operators
#function used to search an initializer or input in the graph
def search_graph(xgraph:GraphProto,find_name:str) -> Union[NodeProto,bool]:
    """Searchs a specific node or initializer with the name 'find_name'.

    :param xgraph: onnx graph where the node or initializer will be searched.
    :type xgraph: GraphProto
    :param find_name: name of the node or initializer to be found.
    :type find_name: str
    """
    for inits in xgraph.initializer:
        if inits.name==find_name:
            return inits
    for inputs in xgraph.input:
        if inputs.name==find_name:
            return inputs
    return False

#onnx doesnt need to be sorted, the list is already in order
@register_func(sorting_funcs,"onnx")
def sorting_onnx(model:GraphProto) -> GraphProto:
    """returns onnx nodes list in order, because of onnx proprieties, the input and output are the same."""
    return model

#preprocessing only consisting on normalising the names of the types and giving the inherited parameters
@register_func(preprocessing,"onnx")
def inherit_attribute_constructor(nodee:NodeProto) -> dict:
    """Constructs a dictionary with parameters of the node; parameters that will be used to construct a class instance of the operator.

    :param node: onnx node to get the information from.
    :type node: NodeProto
    """
    #return dict(op_type=operator_name_normalisation(nodee.op_type,operators),node=nodee,parents=nodee.input)
    return {"op_type":operator_name_normalisation(nodee.op_type,operators),"node":nodee,"parents":nodee.input}

#function that returns the graph and nodes
@register_func(graph_nodes,"onnx")
def pre_model_onnx(model:ModelProto) -> Union[GraphProto,NodeProto]:
    """function that returns the graph and list of nodes

    :param model: onnx model which will be used to get the list of nodes and graph
    :type model: ModelProto
    """
    return model.graph,model.graph.node
