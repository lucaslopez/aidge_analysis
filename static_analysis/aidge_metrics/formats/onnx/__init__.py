"""module containing every method used to get the attributes of operators on the onnx format
"""
from aidge_metrics.formats.onnx.utils import sorting_onnx,inherit_attribute_constructor
from aidge_metrics.formats.onnx.utils import pre_model_onnx

from aidge_metrics.formats.onnx.conv_params import onnx_conv
from aidge_metrics.formats.onnx.pool_params import onnx_pool
from aidge_metrics.formats.onnx.gemm_params import onnx_gemm
from aidge_metrics.formats.onnx.dropout_params import onnx_dropout
from aidge_metrics.formats.onnx.add_params import onnx_add
from aidge_metrics.formats.onnx.reshape_params import onnx_reshape
