"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from aidge_metrics.operator.pool import PoolNode
from aidge_metrics.logic.utils import register_func

@register_func(PoolNode.unique_parameters_functions,"onnx")
def onnx_pool(class_:PoolNode) -> None:
    """Pool operator function to recover additional parameters from the onnx node.

    :param class_: instance of the 'PoolNode' class to add the parameters to.
    :type class_: PoolNode
    """
    attrs = {attr.name : attr for attr in class_.node.attribute}

    class_.kernel=attrs['kernel_shape'].ints

    class_.stride=attrs['strides'].ints

    padding_dims = [0] * 2*len(class_.kernel)
    #code below copied from aidge_onnx/aidge_onnx/node_import/onnx_converters/conv.py
    if 'pads' in attrs:
        # `pads` format should be as follow [x1_begin, x2_begin...x1_end, x2_end,...]
        for i in range(0, len(class_.kernel)):
            padding_dims[2*i] = attrs['pads'].ints[i]
            padding_dims[2*i+1] = attrs['pads'].ints[len(class_.kernel)+i]
    if 'auto_pad' in attrs and attrs['auto_pad'].s in (b'NOTSET', b'SAME_UPPER', b'SAME_LOWER', b'VALID'):
        for i,k in enumerate(class_.kernel):
            padding = k - class_.stride[i]
            floor_half_padding = padding // 2

            if attrs['auto_pad'].s == b'SAME_UPPER':
                padding_dims[2*i] = floor_half_padding
                padding_dims[2*i+1] = padding - floor_half_padding
            elif attrs['auto_pad'].s == b'SAME_LOWER':
                padding_dims[2*i] = padding - floor_half_padding
                padding_dims[2*i+1] = floor_half_padding
    class_.padding=padding_dims
