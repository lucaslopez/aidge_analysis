"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from aidge_metrics.operator.add import AddNode
from aidge_metrics.logic.utils import register_func
from aidge_metrics.formats.onnx.utils import search_graph
#verifying if the operator has weight
@register_func(AddNode.unique_parameters_functions,"onnx")
def onnx_add(class_:AddNode) -> None:
    """add operator function to recover additional parameters from the onnx node.

    :param class_: instance of the 'Addnode' class to add the parameters to.
    :type class_: AddNode.
    """
    weight=search_graph(class_.graph,class_.node.input[1])
    if weight:
        class_.weight_dims=weight.dims
