"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from aidge_metrics.operator.reshape import ReshapeNode
from aidge_metrics.logic.utils import register_func
from aidge_metrics.formats.onnx.utils import search_graph

@register_func(ReshapeNode.unique_parameters_functions,"onnx")
def onnx_reshape(class_:ReshapeNode) -> None:
    """Reshape operator function to recover additional parameters from the aidge node.

    :param class_: instance of the 'ReshapeNode' class to add the parameters to.
    :type class_: ReshapeNode
    """
    attrs = {attr.name : attr for attr in class_.node.attribute}
    if "shape" in attrs:
        class_.new_shape=attrs["shape"].ints
    else:
        init=search_graph(class_.graph,class_.node.input[1])
        #class_.weight_dims=init[onnx_datatypes[init.data_type]]
        class_.new_shape=init.int64_data#FIXME HARDCODED format dependent

    weight=search_graph(class_.graph,class_.node.input[0])
    if weight:
        class_.weight_in_dims=weight.dims
 