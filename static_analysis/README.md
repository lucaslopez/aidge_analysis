<<<<<<< HEAD
# Aidge Metrics

Aidge Metrics is a small project with the objective of measuring different metrics of various CNN models in the Aidge or Onnx format.

Values are 100% theorical and can only be used to give an idea of the size of certain models.

## Usage
###### imports:
Aidge metrics is a simple module based around the usage of the function "aidge_metric"

#### inputs:
 **Required:** 
- model: model to be parsed (in aidge or onnx format)
- init_dimensions: dimensions of the initial data input, defaults to [1, 1, 28, 28]

**Other parameters:**
- model_format: specify if the model is "aidge" or "onnx", this setting is "aidge" by default
- metrics_list: list of the metrics you want to be calculated, this setting is [\"nb_op"] by default
- output_images: bool that indicates if images are to be generated, False by default
- ignored_operators_list: list of operators that will be ignored in graphs and calculations, defaults to []
- ignore_null_metrics: boolean that indicates if null metric values will be ignored, false by default
- output_format: format of the output files, formats supported are png, pdf and svg, svg by default
- save_path: path to where the ouput file will be saved, defaults to None
- plot_per_fig: number of graphs or plots to be generated in a single figure or page, defaults to 4
- graph_types_list: ordered list of the graphs to be drawn in each figure, defaults to [\"barplot","barplot_percentage","pieplot","text"]

#### Outputs:
##### **returns:** 
Ordered list of operators and their values <br />
Example: extract of an detection model: [['Conv_0', 'nb_op', 'Conv', 10035200], ['Conv_0', 'nb_params', 'Conv', 208], ['Add_0', 'nb_op', 'Add', 401408], ['Add_0', 'nb_params', 'Add', 8], ['Relu_0', 'nb_op', 'Relu', 401408], ['Relu_0', 'nb_params', 'Relu', 0], ['Pool_0', 'nb_op', 'Pool', 3211264.0], ['Pool_0', 'nb_params', 'Pool', 0]]

Each item of the list is composed of the following elements: <br />
[**name_of_the_layer , type_of_metric , type_of_layer , value**]

##### Additional outputs
- if *output_images* is set to **True** then an image with 4 graphs for each metric will be generated.
- The image generated is in the SVG format by default but can be set to pdf or png with the *output_format* argument.

## Usage example:

### Aidge model (from onnx):
`resnet = aidge_onnx.load_onnx("your_folder/models/resnet18-v1-7.onnx")` <br />
`aidge_metric(resnet,init_dimensions=[1,3,224,224], metrics_list=["nb_params"],output_images=True, save_path='your_workspace/images_folder/aidge_example')`

### Directy through Onnx
`resnet_onnx=onnx.load("your_folder/models/resnet18-v1-7.onnx")`<br />
`aidge_metric(resnet_onnx,init_dimensions=[1,3,224,224], model_format="onnx", metrics_list=["nb_params"], output_images=True, save_path='your_workspace/images_folder/onnx_example')`


## Algorithm

As said before, Aidge metrics only works by calculating the metric with their theorical formula.

To do this, this tool works by parsing the model, extracting attributes of each layer or node.

Attributes could be input dimensions, size of a kernel filter, output shape, etc

Per example, the theorical number of operations of a convolution layer is dictated by the following formula:<br />
number_of_input_channels* number_of_in_batches* all_out_dimensions* all_kernel_dimensions

And to be able to calculate this we also need to calculate the output_dimensions, where height and width have their own formulas: <br />
h_out = ( (h_in- kernel_h +2* pad) / stride) +1

To be sure to be able to calculate everything aidge metrics extracts every attribute of the layer, calculates other useful information (output dimensions) and then calculates the chosen metric. 

The formulas used for other operators can be found later on this README.

## Operators & Metrics:
### Supported operators

At this point in time, these are the operators supported:

- Convolution
- Add
- Batch norm (inference)
- Dropout (only for onnx format models)
- Flatten
- Gemm or Fully Connected
- Global Average Pool
- Matmul
- Pooling
- Relu
- Reshape (only for onnx format models)
### Supported metrics
And the metrics that can be calculated are the following:
- Number of operations, referred as nb_op
- Number of parameters, referred as nb_params

## Metrics calculation and formula

Here you will find the various formulas used to get the macs of each layer/node

  
You can see the real name and especifications of some of the variables/attributes here:
- in_dims/out_dims: input and output dimensions in a list, using the NCHW convention(Batch , Channel, Height, Width).
- kernel_dims: dimensions of the kernel in form of a list
- in_dims2: makes reference to the second input data in a node

**if an attribute is referred alone, it is a multiplication of the entire list of dimensions**
*Example:* in_dims= n_in* c_in* h_in* w_in
  
Table of formulas:

| /                     | nb_op                                | nb_params                         |
| --------------------- | ------------------------------------ | --------------------------------- |
| Convolution           | c_in\* n_in\* out_dims\* kernel_dims | c_out\* (( kernel_dims\* c_in)+1) |
| Add                   | in_dims                              | 0                                 |
| Dropout               | in_dims\* dropout_ratio              | 0                                 |
| Flatten               | 0                                    | 0                                 |
| Gemm                  | in_dims\* out_dims                   | in_dims\* (out_dims +1)           |
| GAP                   | in_dims                              | 0                                 |
| MatMul                | in_dims\* c_in2                      | 0                                 |
| Pool                  | out_dims\* kernel_dims\* n_in\* c_in | 0                                 |
| Relu                  | in_dims                              | 0                                 |
| Reshape               | 0                                    | 0                                 |
| BatchNorm (inference) | in_dims                              | 0                                 |

## License
As part of the Aidge project, Aidge Metrics is subject to the 2023 CEA-List Copyright and the Eclipse Public License 2.0 available at http://www.eclipse.org/legal/epl-2.0
=======
# Aidge analysis

**Work in progress**

Repository allowing to analysis and benchmark neural network.
>>>>>>> 0fd3f4e21b4448388c7449ed84b2e83d88b11e6d
